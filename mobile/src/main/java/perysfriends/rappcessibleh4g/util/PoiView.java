package perysfriends.rappcessibleh4g.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextPaint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.Toast;


import net.gotev.speech.Speech;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.microedition.khronos.opengles.GL;

import perysfriends.rappcessibleh4g.AppExecutors;
import perysfriends.rappcessibleh4g.R;

/**
 * Inner class for DrawView
 *
 * @author Mike
 */
public class PoiView extends SurfaceView {

    private final float OUT_OF_SCREEN = Float.MIN_VALUE;

    private GlobalApp gApp;
    private List<Poi> visible = null;
    private List<Poi> relevant = null;
    private NinePatchDrawable marker;
    private NinePatchDrawable background = (NinePatchDrawable) getResources().getDrawable(R.drawable.go_straight);
    private double lat = 0, lon = 0;
    private Bitmap bitmap;
    private int hudSize = 400;
    private String locationSource = "---";
    private float filteredOrientation = Float.NaN;
    private float filteredPitch = Float.NaN;
    private float[] orientation = new float[3];
    private final int MIN_DISTANCE = 3;

    // extra info
    private boolean drawExtraInfo = false;
    private Poi poiExtraInfo = null;
    private float xTouch = OUT_OF_SCREEN;
    private float yTouch = OUT_OF_SCREEN;
    private MediaPlayer mediaPlayer;

    public PoiView(Context context, Drawable marker, List<Poi> relevant) {
        super(context);
        setWillNotDraw(false);
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.beep);
        try {
            this.marker = (NinePatchDrawable) marker;
        } catch (Exception e) {
            e.printStackTrace();
            this.marker = (NinePatchDrawable) getResources().getDrawable(R.drawable.go_straight);
        }
        this.relevant = relevant;
        gApp = ((GlobalApp) context.getApplicationContext());
    }

    public void setVisible(List<Poi> visible) {
        this.visible = visible;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public int getHudSize() {
        return hudSize;
    }

    public void setBitmap(Bitmap result) {
        bitmap = result;
    }

    public void setLocationProvider(String provider) {
        this.locationSource = provider;
    }

    public void setFilteredOrientation(float orientation) {
        this.filteredOrientation = orientation;
    }

    public void setFilteredPitch(float pitch) {
        this.filteredPitch = pitch;
    }

    public void setOrientation(float[] orientation) {
        this.orientation = orientation;
    }


    /**
     * Draw on screen.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        //drawDebug(canvas);
        if (gApp.getCurrentLocation() != null) drawHud(canvas);
        drawStops(canvas);
        if (drawExtraInfo) drawExtraInfo(canvas);
    }

    private boolean isWithinCircle(Canvas canvas, int x, int y) {
        int cw = canvas.getWidth();
        int ch = canvas.getHeight();
        int cx = cw / 2;
        int cy = ch / 2;
        int cr = ch / 5;
        return (Math.sqrt(Math.pow((cx - x), 2) + Math.pow((cy - y), 2)) <= cr);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void drawCircle(Canvas canvas, int x, int y) {
        int cw = canvas.getWidth();
        int ch = canvas.getHeight();
        Paint paint = new Paint();
        if (isWithinCircle(canvas, x, y))
            paint.setColor(Color.GREEN);
        else {
            paint.setColor(Color.RED);
            if (!mediaPlayer.isPlaying()) {
                PlaybackParams params = new PlaybackParams();
                float result = ((float)x/(float)ch);
                Log.i("hola", "drawCircle: " + result);
                float pitch = 1F;
                float speed = 0F;
                if(result > 1) {
                    pitch = 2F;
                    if(result > 1 && result <= 2)
                        speed = 1F;
                    else if(result > 2 && result <= 3)
                        speed = 1.5F;
                    else if(result > 3)
                        speed = 2F;
                }
                else{
                    if(result < 1 && result >= 0)
                        speed = 1F;
                    else if(result < 0 && result <= -1)
                        speed = 1.5F;
                    else if(result > -1)
                        speed = 2F;
                }
                params.setPitch(pitch);
                params.setSpeed(speed);
                mediaPlayer.setPlaybackParams(params);
                mediaPlayer.start();
            }
        }
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(25);
        canvas.drawCircle(cw / 2, ch / 2, ch / 5, paint);

    }

    /**
     * Take care of touch event.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (drawExtraInfo) {
                drawExtraInfo = false;
                poiExtraInfo = null;
                xTouch = OUT_OF_SCREEN;
                yTouch = OUT_OF_SCREEN;
            } else {
                drawExtraInfo = true;
                xTouch = event.getX();
                yTouch = event.getY();
            }
        }
        return true;
    }

    /**
     * Method to draw stops on canvas.
     *
     * @param canvas Canvas we draw on.
     */
    private void drawStops(Canvas canvas) {
        visible = gApp.getStops();
        if (visible == null || visible.size() == 0 || visible.get(0) == null)
            return;

        // setup text paint
        float textSize = 28f;        // initial minimum // ZMENA
        float textSizeDiff = 32f;    // total max is textSize + textSizeDiff // ZMENA
        TextPaint textPaint = new TextPaint();
        textPaint.setARGB(255, 255, 255, 255);
        textPaint.setAntiAlias(true);

        // calculate data related to width
        int cw = canvas.getWidth();
        double widthFOV = (double) cw / (double) gApp.getFOV();

        // calculate variables related to height
        int ch = canvas.getHeight();
        Location currentLocation = gApp.getCurrentLocation();
        if (currentLocation == null)
            return;
        float[] distanceBetween = new float[5];
        Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(), visible.get(0).getLat(), visible.get(0).getLon(), distanceBetween);
        double heightDistance = (double) ch / (double) distanceBetween[0];

        Poi p = visible.get(0);
        if (p.getDistance() < MIN_DISTANCE) {
            visible.remove(p);
            if (visible.size() > 0) {
                p = visible.get(0);
                Speech.getInstance().say(p.getDescription());
            }
        }
        double angleDiff = p.getAngleDiff() + (double) gApp.getFOVHALF();

        // manually calculate dimensions
        int maxSize = ch - (int) (ch * 0.4);
        int realSize;
        if (p.getDistance() == 0)
            realSize = MIN_DISTANCE * maxSize;
        else
            realSize = (MIN_DISTANCE * maxSize / p.getDistance());
        if (realSize <= ch * 0.1) {
            realSize = (int) (ch * 0.1);
        }
        double height = 50 / (Math.tan(filteredPitch) * (double) p.getDistance());
        int realHeight = (int) (MIN_DISTANCE * maxSize / height);
        int left = (int) (widthFOV * angleDiff);
        int top;
        top = (ch / 2) - (((int) textSize + (int) textSize + 10) / 2);
        textSize += textSizeDiff * (float) top / (float) ch;
        textPaint.setTextSize(textSize);
//        top = ch / 2 - realSize / 2 - realHeight;
        int right = left + realSize;
        int bottom = top + realSize;

        // determine size; fix for very short stop names
        int initialRight;
        Rect npdBounds = new Rect();
        StringBuilder sb = new StringBuilder(String.valueOf(p.getDistance()));

        textPaint.getTextBounds(p.getName(), 0, p.getName().length(), npdBounds);
        initialRight = npdBounds.right;
        sb.append("m");
        textPaint.getTextBounds(sb.toString(), 0, sb.length(), npdBounds);

        if (initialRight > npdBounds.right)
            npdBounds.right = initialRight;

        // put everything together
        npdBounds.left = left - (right - left) / 2;
        npdBounds.top = top;
        npdBounds.right = npdBounds.left + (right - left);
        npdBounds.bottom = bottom;

        NinePatchDrawable newMarker;
        newMarker = (NinePatchDrawable) getResources().getDrawable(R.drawable.go_straight);

        drawCircle(canvas, left + (realSize / 2), ch / 2);
        newMarker.setBounds(npdBounds);
        newMarker.draw(canvas);

        // draw text
        //canvas.drawText(p.getName(), left + 10 - shiftLeft, top + textSize + 5 - shiftUp, textPaint);
        //canvas.drawText(String.valueOf(p.getDistance()) + "m", left + 10 - shiftLeft, top + textSize + 5 + textSize - shiftUp, textPaint);

        // end of cycle; "remove" touch
        xTouch = OUT_OF_SCREEN;
        yTouch = OUT_OF_SCREEN;
    }

    /**
     * Method to draw stops on canvas.
     *
     * @param canvas Canvas we draw on.
     */
    private void drawExtraInfo(Canvas canvas) {
        if (poiExtraInfo == null)
            return;

        int x = 0;
        int y = 0;
        int width = canvas.getWidth();
        int height = 140;
        int textSize = 45;

        Rect npdBounds = new Rect(x, y, x + width, y + height);
        background.setBounds(npdBounds);
        background.draw(canvas);

        TextPaint textPaint = new TextPaint();
        textPaint.setARGB(255, 255, 255, 255);
        textPaint.setTextSize(textSize);
        textPaint.setAntiAlias(true);

        canvas.drawText("Nombre: " + poiExtraInfo.getName(), x + 20, y + 12 + textSize, textPaint);
        //canvas.drawText("Latitude: " + poiExtraInfo.getLat(), x + 10, y + 5 + 60, textPaint);
        canvas.drawText("Distancia: " + poiExtraInfo.getDistance() + " m", x + 20, y + 17 + textSize * 2, textPaint);
        //canvas.drawText("Lines: " + poiExtraInfo.getDescription(), x + 10, y + 5 + 120, textPaint);
    }

    /**
     * Draw HUD
     * This method is only for debugging.
     *
     * @param canvas Canvas we draw on.
     */
    @SuppressLint({"FloatMath", "FloatMath"})
    private void drawHud(Canvas canvas) {
        relevant = gApp.getRelevant();
        Location cloc = gApp.getCalcLocation();

        Paint paintBg = new Paint();
        paintBg.setARGB(255, 0, 0, 0);
        Paint paintGreen = new Paint();
        paintGreen.setARGB(255, 0, 255, 0);
        paintGreen.setStrokeWidth(3);
        Paint paintRed = new Paint();
        paintRed.setARGB(255, 255, 0, 0);
        paintRed.setStrokeWidth(3);
        Paint paintBlue = new Paint();
        paintBlue.setARGB(255, 0, 0, 255);
        paintBlue.setStrokeWidth(2);

        // center - kind of
        float fromLeft = canvas.getWidth() - (hudSize / 2);
        float fromTop = hudSize / 2;

        // draw black border
        canvas.drawRect(canvas.getWidth() - (hudSize + 2), 0, canvas.getWidth(), (hudSize + 2), paintBg);

        // draw white bg or a map
        paintBg.setARGB(255, 255, 255, 255);
        if (bitmap == null) {
            canvas.drawRect(canvas.getWidth() - hudSize, 0, canvas.getWidth(), hudSize, paintBg);
        } else {
            canvas.drawBitmap(bitmap, canvas.getWidth() - hudSize, 0, paintBg);
        }

        // draw direction
        float angleRad = (float) ((Math.PI / 2.0) - filteredOrientation);
        float userX = (float) Math.cos(angleRad);
        float userY = (float) -Math.sin(angleRad);
        canvas.drawLine(fromLeft, fromTop, userX * (hudSize / 2) + fromLeft, userY * (hudSize / 2) + fromTop, paintBlue);

        // draw stops
        if (relevant != null && cloc != null) {
            for (Poi p : relevant) {
                float pX = (float) ((p.getLon() - cloc.getLongitude()) / gApp.getD() * 70);
                float pY = (float) -((p.getLat() - cloc.getLatitude()) / gApp.getD() * 85);

                if (p.isVisible()) {
                    canvas.drawPoint(pX + fromLeft, pY + fromTop, paintGreen);
                } else {
                    canvas.drawPoint(pX + fromLeft, pY + fromTop, paintRed);
                }
            }
        }
    }

    /**
     * Method to draw debug info onto canvas
     *
     * @param canvas
     */
    @SuppressWarnings("unused")
    private void drawDebug(Canvas canvas) {
        TextPaint textPaint = new TextPaint();
        textPaint.setARGB(255, 255, 43, 43);
        textPaint.setTextSize(30);

        canvas.drawText("Location source: " + locationSource, 10, 30, textPaint);
        canvas.drawText("Latitude: " + lat, 10, 60, textPaint);
        canvas.drawText("Longitude: " + lon, 10, 90, textPaint);
        canvas.drawText(String.format("Azimuth: %5.2f", Util.normalize(Util.rad2deg(filteredOrientation))), 10, 120, textPaint);
        canvas.drawText(String.format("Pitch: %5.2f", Util.normalize(Util.rad2deg(filteredPitch))), 10, 150, textPaint);
        canvas.drawText(String.format("Roll: %5.2f", Util.rad2deg(orientation[2])), 10, 180, textPaint);

        if (visible != null) {
            int startY = 210;
            int max = 7;

            if (max > visible.size()) {
                max = visible.size();
            }

            for (int i = 0; i < max; i++) {
                Poi p = visible.get(i);
                canvas.drawText(p.getName() + " - " + p.getDescription(), 10, startY, textPaint);
                startY += 30;
            }
        }
    }
}
