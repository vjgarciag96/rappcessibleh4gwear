package perysfriends.rappcessibleh4g.remote;

import perysfriends.rappcessibleh4g.model.RouteResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by victor on 4/03/18.
 */

public interface RouteService {
    @GET("/route") Call<RouteResponse> getRoute(@Query("from") String from,
                                                @Query("to") String to,
                                                @Query("avoid_stairs") boolean avoidStairs);
}
