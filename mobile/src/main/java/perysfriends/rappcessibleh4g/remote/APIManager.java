package perysfriends.rappcessibleh4g.remote;

/**
 * Created by victor on 4/03/18.
 */

public class APIManager {
    public static final String BASE_URL = "https://overpassproxy.herokuapp.com";

    public static RouteService getRouteService(){
        return RetrofitClient.getClient(BASE_URL).create(RouteService.class);
    }

}
