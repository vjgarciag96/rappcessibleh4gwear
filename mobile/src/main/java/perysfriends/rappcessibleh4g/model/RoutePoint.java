package perysfriends.rappcessibleh4g.model;

import java.io.Serializable;

/**
 * Created by victor on 5/03/18.
 */

public class RoutePoint implements Serializable {

    private double latitude;
    private double longitude;

    public RoutePoint(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
