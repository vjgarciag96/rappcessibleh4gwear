
package perysfriends.rappcessibleh4g.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoundingBox {

    @SerializedName("lr")
    @Expose
    private Lr lr;
    @SerializedName("ul")
    @Expose
    private Ul ul;

    public Lr getLr() {
        return lr;
    }

    public void setLr(Lr lr) {
        this.lr = lr;
    }

    public Ul getUl() {
        return ul;
    }

    public void setUl(Ul ul) {
        this.ul = ul;
    }

}
