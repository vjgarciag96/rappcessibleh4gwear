
package perysfriends.rappcessibleh4g.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {

    @SerializedName("dragPoint")
    @Expose
    private Boolean dragPoint;
    @SerializedName("displayLatLng")
    @Expose
    private DisplayLatLng displayLatLng;
    @SerializedName("adminArea4")
    @Expose
    private String adminArea4;
    @SerializedName("adminArea5")
    @Expose
    private String adminArea5;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("adminArea1")
    @Expose
    private String adminArea1;
    @SerializedName("adminArea3")
    @Expose
    private String adminArea3;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("sideOfStreet")
    @Expose
    private String sideOfStreet;
    @SerializedName("geocodeQualityCode")
    @Expose
    private String geocodeQualityCode;
    @SerializedName("adminArea4Type")
    @Expose
    private String adminArea4Type;
    @SerializedName("linkId")
    @Expose
    private Integer linkId;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("adminArea5Type")
    @Expose
    private String adminArea5Type;
    @SerializedName("geocodeQuality")
    @Expose
    private String geocodeQuality;
    @SerializedName("adminArea1Type")
    @Expose
    private String adminArea1Type;
    @SerializedName("adminArea3Type")
    @Expose
    private String adminArea3Type;
    @SerializedName("latLng")
    @Expose
    private LatLng latLng;

    public Boolean getDragPoint() {
        return dragPoint;
    }

    public void setDragPoint(Boolean dragPoint) {
        this.dragPoint = dragPoint;
    }

    public DisplayLatLng getDisplayLatLng() {
        return displayLatLng;
    }

    public void setDisplayLatLng(DisplayLatLng displayLatLng) {
        this.displayLatLng = displayLatLng;
    }

    public String getAdminArea4() {
        return adminArea4;
    }

    public void setAdminArea4(String adminArea4) {
        this.adminArea4 = adminArea4;
    }

    public String getAdminArea5() {
        return adminArea5;
    }

    public void setAdminArea5(String adminArea5) {
        this.adminArea5 = adminArea5;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAdminArea1() {
        return adminArea1;
    }

    public void setAdminArea1(String adminArea1) {
        this.adminArea1 = adminArea1;
    }

    public String getAdminArea3() {
        return adminArea3;
    }

    public void setAdminArea3(String adminArea3) {
        this.adminArea3 = adminArea3;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSideOfStreet() {
        return sideOfStreet;
    }

    public void setSideOfStreet(String sideOfStreet) {
        this.sideOfStreet = sideOfStreet;
    }

    public String getGeocodeQualityCode() {
        return geocodeQualityCode;
    }

    public void setGeocodeQualityCode(String geocodeQualityCode) {
        this.geocodeQualityCode = geocodeQualityCode;
    }

    public String getAdminArea4Type() {
        return adminArea4Type;
    }

    public void setAdminArea4Type(String adminArea4Type) {
        this.adminArea4Type = adminArea4Type;
    }

    public Integer getLinkId() {
        return linkId;
    }

    public void setLinkId(Integer linkId) {
        this.linkId = linkId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAdminArea5Type() {
        return adminArea5Type;
    }

    public void setAdminArea5Type(String adminArea5Type) {
        this.adminArea5Type = adminArea5Type;
    }

    public String getGeocodeQuality() {
        return geocodeQuality;
    }

    public void setGeocodeQuality(String geocodeQuality) {
        this.geocodeQuality = geocodeQuality;
    }

    public String getAdminArea1Type() {
        return adminArea1Type;
    }

    public void setAdminArea1Type(String adminArea1Type) {
        this.adminArea1Type = adminArea1Type;
    }

    public String getAdminArea3Type() {
        return adminArea3Type;
    }

    public void setAdminArea3Type(String adminArea3Type) {
        this.adminArea3Type = adminArea3Type;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

}
