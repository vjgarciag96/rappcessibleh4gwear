package perysfriends.rappcessibleh4g.model;

import java.util.List;

/**
 * Created by victor on 9/03/18.
 */

public class RoutePointsWithIndication {

    private RoutePointWithIndication start;
    private RoutePointWithIndication end;
    private List<RoutePointWithIndication> waypoints;

    public RoutePointsWithIndication(RoutePointWithIndication start,
                                     RoutePointWithIndication end,
                                     List<RoutePointWithIndication> waypoints) {
        this.start = start;
        this.end = end;
        this.waypoints = waypoints;
    }

    public RoutePointWithIndication getStart() {
        return start;
    }

    public void setStart(RoutePointWithIndication start) {
        this.start = start;
    }

    public RoutePointWithIndication getEnd() {
        return end;
    }

    public void setEnd(RoutePointWithIndication end) {
        this.end = end;
    }

    public List<RoutePointWithIndication> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<RoutePointWithIndication> waypoints) {
        this.waypoints = waypoints;
    }
}
