
package perysfriends.rappcessibleh4g.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Maneuver {

    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("streets")
    @Expose
    private List<String> streets = null;
    @SerializedName("narrative")
    @Expose
    private String narrative;
    @SerializedName("turnType")
    @Expose
    private Integer turnType;
    @SerializedName("startPoint")
    @Expose
    private StartPoint startPoint;
    @SerializedName("index")
    @Expose
    private Integer index;
    @SerializedName("formattedTime")
    @Expose
    private String formattedTime;
    @SerializedName("directionName")
    @Expose
    private String directionName;
    @SerializedName("maneuverNotes")
    @Expose
    private List<Object> maneuverNotes = null;
    @SerializedName("linkIds")
    @Expose
    private List<Object> linkIds = null;
    @SerializedName("signs")
    @Expose
    private List<Object> signs = null;
    @SerializedName("mapUrl")
    @Expose
    private String mapUrl;
    @SerializedName("transportMode")
    @Expose
    private String transportMode;
    @SerializedName("attributes")
    @Expose
    private Integer attributes;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("direction")
    @Expose
    private Integer direction;

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public List<String> getStreets() {
        return streets;
    }

    public void setStreets(List<String> streets) {
        this.streets = streets;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public Integer getTurnType() {
        return turnType;
    }

    public void setTurnType(Integer turnType) {
        this.turnType = turnType;
    }

    public StartPoint getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(StartPoint startPoint) {
        this.startPoint = startPoint;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getFormattedTime() {
        return formattedTime;
    }

    public void setFormattedTime(String formattedTime) {
        this.formattedTime = formattedTime;
    }

    public String getDirectionName() {
        return directionName;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    public List<Object> getManeuverNotes() {
        return maneuverNotes;
    }

    public void setManeuverNotes(List<Object> maneuverNotes) {
        this.maneuverNotes = maneuverNotes;
    }

    public List<Object> getLinkIds() {
        return linkIds;
    }

    public void setLinkIds(List<Object> linkIds) {
        this.linkIds = linkIds;
    }

    public List<Object> getSigns() {
        return signs;
    }

    public void setSigns(List<Object> signs) {
        this.signs = signs;
    }

    public String getMapUrl() {
        return mapUrl;
    }

    public void setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
    }

    public String getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    public Integer getAttributes() {
        return attributes;
    }

    public void setAttributes(Integer attributes) {
        this.attributes = attributes;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

}
