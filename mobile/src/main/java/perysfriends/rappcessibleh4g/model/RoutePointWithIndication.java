package perysfriends.rappcessibleh4g.model;

/**
 * Created by victor on 9/03/18.
 */

public class RoutePointWithIndication extends RoutePoint {

    private String indication;

    public RoutePointWithIndication(double latitude, double longitude, String indication) {
        super(latitude, longitude);
        this.indication = indication;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }
}
