package perysfriends.rappcessibleh4g.map;

import android.util.Log;

import com.google.android.gms.maps.model.UrlTileProvider;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by victor on 4/03/18.
 */

public class CustomUrlTileProvider extends UrlTileProvider {
    private static final String CARTO_MAP_URL_FORMAT =
            "https://api.mapbox.com/styles/v1/jpery/cjeh00kaj63wj2rqtp26qatam/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoianBlcnkiLCJhIjoiY2o4dTRnbzI3MHMzdjJwcGxjd3JqdXhjaCJ9.m55TdldUtwX2Od_Pp4DbNQ";

    public CustomUrlTileProvider(int width, int height) {
        super(width, height);
    }

    @Override
    public URL getTileUrl(int x, int y, int zoom) {
        try {
            return new URL(CARTO_MAP_URL_FORMAT.replace("{z}", "" + zoom).replace("{x}", "" + x)
                    .replace("{y}", "" + y));
        } catch (MalformedURLException e) {
            Log.i("map", "getTileUrl: " + e.getMessage());
        }
        return null;
    }
}
