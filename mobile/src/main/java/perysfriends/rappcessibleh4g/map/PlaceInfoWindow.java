package perysfriends.rappcessibleh4g.map;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import perysfriends.rappcessibleh4g.MainActivity;
import perysfriends.rappcessibleh4g.PlaceMarker;
import perysfriends.rappcessibleh4g.R;

/**
 * Created by victor on 8/03/18.
 */

public class PlaceInfoWindow implements GoogleMap.InfoWindowAdapter {

    private static final String TAG = "PlaceInfoWindow";
    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/streetview?size=600x400&location={location}&key={key}";

    private View infoWindow;
    private TextView markerTitle;
    private TextView markerDescription;
    private TextView placeDetailIndication;
    private ImageView markerImage;
    private Context context;

    public PlaceInfoWindow(Context context) {
        this.context = context;
        infoWindow = ((MainActivity) context).getLayoutInflater().inflate(R.layout.place_info_window, null);
        markerTitle = infoWindow.findViewById(R.id.markerTitle);
        markerDescription = infoWindow.findViewById(R.id.markerDescription);
        placeDetailIndication = infoWindow.findViewById(R.id.bar_detail_indication);
        markerImage = infoWindow.findViewById(R.id.markerImage);
    }


    @Override
    public View getInfoWindow(Marker marker) {
        PlaceMarker placeMarker = ((MainActivity) context).getPlaceMarker();
        Picasso picasso = Picasso.with(context);
        markerTitle.setText(marker.getTitle().toUpperCase());
        markerDescription.setText(marker.getSnippet());
        if (marker.getTitle().equals("Ubicación")) {
            String imageUrl = "https://cdn-images-1.medium.com/max/1280/1*kMnsh6rR8RIAX7wi08wH-g.jpeg";
            if (((MainActivity)context).getLocationLoaded())
                picasso.load(imageUrl)
                        .into(markerImage);
            else
                picasso.load(imageUrl)
                        .into(markerImage, new UserMarkerCallback(marker, context));
            placeDetailIndication.setText(" ");
        }
        else if(marker.getSnippet().equals("Sitio seleccionado")){
            String imageUrl = "http://marketingmix.co.uk/content/uploads/marketing-mix-place.jpg";
            if (((MainActivity)context).isLongMarkedLoaded())
                picasso.load(imageUrl)
                        .into(markerImage);
            else
                picasso.load(imageUrl)
                        .into(markerImage, new LongMarkerCallback(marker, context));
            placeDetailIndication.setText("NAVEGAR A ESTE SITIO >");
        }
        else {
            if (placeMarker.isImageLoaded())
                picasso.load(BASE_URL.replace("{key}", context.getString(R.string.maps_api_key)).replace("{location}", placeMarker.getPosition().latitude + "," + placeMarker.getPosition().longitude))
                        .into(markerImage);
            else
                picasso.load(BASE_URL.replace("{key}", context.getString(R.string.maps_api_key)).replace("{location}", placeMarker.getPosition().latitude + "," + placeMarker.getPosition().longitude))
                        .into(markerImage, new MarkerCallback(marker, placeMarker));
            placeDetailIndication.setText("NAVEGAR A ESTE SITIO >");
        }

        return infoWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return infoWindow;
    }

    static class MarkerCallback implements Callback {
        private Marker marker;
        private PlaceMarker placeMarker;

        MarkerCallback(Marker marker, PlaceMarker placeMarker) {
            this.marker = marker;
            this.placeMarker = placeMarker;
        }

        @Override
        public void onError() {
            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                this.placeMarker.setImageLoaded(true);
                marker.showInfoWindow();
            }
        }
    }

    static class UserMarkerCallback implements Callback {
        private Marker marker;
        private Context context;

        public UserMarkerCallback(Marker marker, Context context) {
            this.marker = marker;
            this.context = context;
        }

        @Override
        public void onError() {
            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
            ((MainActivity)context).setLocationLoaded(true);
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                ((MainActivity)context).setLocationLoaded(true);
                marker.showInfoWindow();
            }
        }
    }

    static class LongMarkerCallback implements Callback {
        private Marker marker;
        private Context context;

        public LongMarkerCallback(Marker marker, Context context) {
            this.marker = marker;
            this.context = context;
        }

        @Override
        public void onError() {
            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
            ((MainActivity)context).setLongMarkedLoaded(true);
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                ((MainActivity)context).setLongMarkedLoaded(true);
                marker.showInfoWindow();
            }
        }
    }
}
