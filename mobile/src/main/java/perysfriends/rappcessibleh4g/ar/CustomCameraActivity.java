package perysfriends.rappcessibleh4g.ar;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import net.gotev.speech.Speech;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import perysfriends.rappcessibleh4g.R;
import perysfriends.rappcessibleh4g.model.RoutePoint;
import perysfriends.rappcessibleh4g.model.RoutePointWithIndication;
import perysfriends.rappcessibleh4g.model.RoutePoints;
import perysfriends.rappcessibleh4g.model.RoutePointsWithIndication;
import perysfriends.rappcessibleh4g.util.GlobalApp;
import perysfriends.rappcessibleh4g.util.Poi;
import perysfriends.rappcessibleh4g.util.PoiView;
import perysfriends.rappcessibleh4g.util.Util;
import perysfriends.rappcessibleh4g.view.CameraView;

/**
 * CameraActivity class
 * This class combines CameraView and DrawView and
 * implements location and sensor listeners (accelerometer, magnetic field)
 *
 * @author Mike
 */
public class CustomCameraActivity extends AppCompatActivity
        implements LocationListener, SensorEventListener, ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = "AR";

    private CameraView cameraView;
    private PoiView poiView;
    private LocationManager mLocationManager;
    private SensorManager mSensorManager;
    private GlobalApp gApp;

    // orientation, location, calculations
    private List<Poi> relevant = null;
    private float[] accelerometerValues = new float[3];
    private float[] magneticFieldValues = new float[3];
    private boolean deviceHasMagneticField;
    private boolean deviceHasAccelerometer;
    private float filteredOrientation = Float.NaN;
    private float filteredPitch = Float.NaN;

    private GoogleApiClient googleApiClient;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    boolean firstFix = true;


    /**
     * Setup required services in onCreate
     * Set window parameters
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Speech.init(this);
        Intent intent = getIntent();
        String serializedRoute = intent.getStringExtra("route");
        RoutePointsWithIndication routePoints = new Gson().fromJson(serializedRoute, RoutePointsWithIndication.class);
        if (routePoints != null) {
            List<Poi> pois = new ArrayList<>();
            for (RoutePointWithIndication routePoint : routePoints.getWaypoints()) {
                pois.add(new Poi(routePoint.getLatitude(), routePoint.getLongitude(), routePoint.getIndication()));
            }
            Speech.getInstance().say(routePoints.getWaypoints().get(0).getIndication());
            gApp = ((GlobalApp) getApplicationContext());
            gApp.setStops(pois);
            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        onLocationChanged(location);
                    }
                }
            };
            loadGoogleApiClient();
        }
    }

    /**
     * Set Layout as a combination of 2 views on top of each other
     * Open camera and pass it as parameter to CameraView
     */
    @SuppressWarnings("deprecation")
    private void LayoutSetup() {
        Camera camera = Camera.open();
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        cameraView = new CameraView(this, camera);
        frameLayout.addView(cameraView);
        poiView = new PoiView(this, getResources().getDrawable(R.drawable.go_straight), relevant);
        frameLayout.addView(poiView);
        setContentView(frameLayout);
    }

    /**
     * Take care of camera and unregister listeners
     */
    protected void onPause() {
        super.onPause();
        if (cameraView != null) {
            cameraView.onPause();
            cameraView = null;
        }
        mLocationManager.removeUpdates(this);
        mSensorManager.unregisterListener(this);
    }

    /**
     * Call layout setup
     * Request location and sensor updates
     */
    protected void onResume() {
        super.onResume();
        LayoutSetup();
        deviceHasAccelerometer = mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        if (deviceHasAccelerometer) {
            Log.i("Sensor", "Device HAS Accelerometer");
        } else {
            Log.i("Sensor", "Device has NOT Accelerometer");
        }
        deviceHasMagneticField = mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
        if (deviceHasMagneticField) {
            Log.i("Sensor", "Device HAS Magnetometer");
        } else {
            Log.i("Sensor", "Device has NOT Magnetometer");
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            startLocationUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
    }

    private void loadGoogleApiClient() {
        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void startLocationUpdates() throws SecurityException {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest settingsRequest = builder.build();
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(settingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, "onFailure: ");
                    }
                });
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationProviderClient.requestLocationUpdates(LocationRequest.create(), locationCallback, null);
    }

    private void stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_COARSE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Toast.makeText(this, "Tienes que proporcionar permisos para usar la app", Toast.LENGTH_SHORT).show();
            else
                startLocationUpdates();
        }
    }

    /**
     * Update location and relevant stops if necessary.
     *
     * @param location       Latest location update
     * @param updateRelevant Update flag
     */
    private void updateLocation(Location location, boolean updateRelevant) {
        gApp.setCurrentLocation(location);
        if (updateRelevant) {
            gApp.findRelevantStops();
            relevant = gApp.getRelevant();
            if (relevant.size() == 0) {
                Toast.makeText(getApplicationContext(), "No relevant stops found ...", Toast.LENGTH_LONG).show();
            }
        }
        poiView.setLocationProvider(location.getProvider());
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        poiView.setLat(lat);
        poiView.setLon(lon);
        List<Poi> stops = gApp.getStops();
        String url = String.format("http://maps.googleapis.com/maps/api/staticmap?center=%s,%s&size=%dx%d&sensor=false&maptype=roadmap&zoom=17&key=AIzaSyD83J8Wo2Yl49fhF4fWc3mNbheq1AHiFLQ",
                Double.toString(lat).replace(',', '.'), Double.toString(lon).replace(',', '.'), poiView.getHudSize(), poiView.getHudSize());
        url += "&path=color:0x0000ff|weight:5";
        for (Poi stop : stops) {
            url += "|" + stop.getLat() + "," + stop.getLon();
        }
        for (Poi stop : stops) {
            url += "&markers=color:red|" + stop.getLat() + "," + stop.getLon();
        }
        new DownloadImageTask().execute(url);
    }

    /**
     * AsyncTask to download static map from google servers
     */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(in);
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            poiView.setBitmap(result);
        }
    }

    // Location Listener Methods - START

    /**
     * This method takes care of location events
     * Store GPS coordinates if possible, otherwise use network
     */
    public void onLocationChanged(Location location) {
        if (gApp.getCurrentLocation() == null || firstFix) {
            updateLocation(location, true);
            firstFix = false;
        } else {
            if (location.distanceTo(gApp.getCalcLocation()) >= gApp.getTRESHOLD()) {
                updateLocation(location, true);
            } else {
                updateLocation(location, false);
            }
        }
        poiView.invalidate();
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    // Location Listener Methods - END

    // Sensor Event Listener Methods - START

    /**
     * This method takes care of location events
     * Recalculate azimuth and visible stops
     */
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER && deviceHasAccelerometer) {
            System.arraycopy(event.values, 0, accelerometerValues, 0, event.values.length);
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD && deviceHasMagneticField) {
            System.arraycopy(event.values, 0, magneticFieldValues, 0, event.values.length);
        }
        float[] R = new float[9];
        SensorManager.getRotationMatrix(R, null, accelerometerValues, magneticFieldValues);
        // needs to be remapped for landscape mode
        SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_X, SensorManager.AXIS_Z, R);
        float[] orientation = new float[3];
        SensorManager.getOrientation(R, orientation);

        // orientation smoothing
        float calcOrientation = orientation[0];
        float calcPitch = (float) Math.asin(-R[7]);

        poiView.setOrientation(orientation);
        float alpha = 0.05f;
        if (Float.isNaN(filteredOrientation))
            filteredOrientation = calcOrientation;
        float diffOrientation = calcOrientation - filteredOrientation;
        if (diffOrientation > Math.PI)
            diffOrientation -= 2 * Math.PI;
        filteredOrientation = (alpha * diffOrientation + filteredOrientation);
        if (Float.isNaN(filteredPitch))
            filteredPitch = calcPitch;
        float diffPitch = calcPitch - filteredPitch;
        while (diffPitch > Math.PI)
            diffPitch -= 2 * Math.PI;
        filteredPitch = (alpha * diffPitch + filteredPitch);

        poiView.setFilteredOrientation(filteredOrientation);
        poiView.setFilteredPitch(filteredPitch);
        // find visible stops according to azimuth
        gApp.findVisibleStops(Util.rad2deg(filteredOrientation), Util.rad2deg(filteredPitch));
        poiView.setVisible(gApp.getVisible());
        poiView.invalidate();
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }
    // Sensor Event Listener Methods - END
}