package perysfriends.rappcessibleh4g;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.GeoDataApi;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.internal.zzw;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;

import net.gotev.speech.GoogleVoiceTypingDisabledException;
import net.gotev.speech.Speech;
import net.gotev.speech.SpeechDelegate;
import net.gotev.speech.SpeechRecognitionNotAvailable;
import net.gotev.speech.TextToSpeechCallback;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import perysfriends.rappcessibleh4g.ar.CustomCameraActivity;
import perysfriends.rappcessibleh4g.map.CustomUrlTileProvider;
import perysfriends.rappcessibleh4g.map.LatLngBoundsCalculator;
import perysfriends.rappcessibleh4g.map.PlaceInfoWindow;
import perysfriends.rappcessibleh4g.model.Leg;
import perysfriends.rappcessibleh4g.model.Maneuver;
import perysfriends.rappcessibleh4g.model.Route;
import perysfriends.rappcessibleh4g.model.RoutePoint;
import perysfriends.rappcessibleh4g.model.RoutePointWithIndication;
import perysfriends.rappcessibleh4g.model.RoutePoints;
import perysfriends.rappcessibleh4g.model.RoutePointsWithIndication;
import perysfriends.rappcessibleh4g.model.RouteResponse;
import perysfriends.rappcessibleh4g.model.StartPoint;
import perysfriends.rappcessibleh4g.remote.APIManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,
        ActivityCompat.OnRequestPermissionsResultCallback,
        SpeechDelegate {

    private static final String TAG = "MainActivity";

    private static final int REQUEST_CAMERA_SERVICE = 1;

    private static final String WAYPOINTS_PATH = "/waypoint";
    private static final String WAYPOINT_ROUTE_KEY = "waypoint-route-lat";
    private static final String START_ACTIVITY_PATH = "/start-activity";

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    public static final int PERMISSION_REQUEST_RECORD_AUDIO = 2;

    private GoogleMap map;
    private TileOverlay tileOverlay;

    private GoogleApiClient googleApiClient;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private Marker userLocationMarker;
    private Circle accuracyCircle;

    private LatLng userLocation;
    private PlaceMarker placeMarker;
    private boolean locationLoaded;
    private boolean longMarkedLoaded;
    private Marker placePosMarker;

    private PlaceAutocompleteFragment autocompleteFragment;

    private String serializedRoute;
    private String serializedRouteForAR;


    Polyline routePolilyne;

    @BindView(R.id.accessible_switch) SwitchCompat switchCompat;
    private boolean accesibleChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        locationLoaded = false;
        longMarkedLoaded = false;

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                accesibleChecked = isChecked;
            }
        });
        loadGoogleApiClient();
        loadGoogleMap();
        Speech.init(this);
        checkPermissions();

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    if (map != null) {
                        userLocation = new LatLng(location.getLatitude(),
                                location.getLongitude());
                        if (userLocationMarker == null) {
                            userLocationMarker = map.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
                                    .position(userLocation)
                                    .zIndex(1000).anchor(0.5F, 0.5F)
                                    .title("Ubicación")
                                    .snippet("Tu posición"));
                            CircleOptions accuracyCircleOptions = new CircleOptions().center(userLocation).radius(location.getAccuracy()).zIndex(100).fillColor(0x3c4187f5).strokeWidth(0);
                            accuracyCircle = map.addCircle(accuracyCircleOptions);
                        } else {
                            userLocationMarker.setPosition(userLocation);
                            accuracyCircle.setCenter(userLocation);
                            accuracyCircle.setRadius(location.getAccuracy());
                        }
                        if (autocompleteFragment != null)
                            autocompleteFragment.setBoundsBias(LatLngBoundsCalculator.boundsWithCenterAndLatLngDistance(userLocation, 50000, 50000));
                    }
                }
            }
        };
    }

    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] accessCoarseLocationPermission = {Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, accessCoarseLocationPermission, PERMISSION_REQUEST_COARSE_LOCATION);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            startLocationUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
    }

    public boolean isLongMarkedLoaded() {
        return longMarkedLoaded;
    }

    public void setLongMarkedLoaded(boolean longMarkedLoaded) {
        this.longMarkedLoaded = longMarkedLoaded;
    }

    private void startLocationUpdates() throws SecurityException {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest settingsRequest = builder.build();
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(settingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, "onFailure: ");
                    }
                });
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationProviderClient.requestLocationUpdates(LocationRequest.create(), locationCallback, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_COARSE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Toast.makeText(this, "Tienes que proporcionar permisos para usar la app", Toast.LENGTH_SHORT).show();
            else
                startLocationUpdates();
        }
        if (requestCode == PERMISSION_REQUEST_RECORD_AUDIO) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Toast.makeText(this, "No has habilitado las funciones de audio", Toast.LENGTH_SHORT).show();
            else
                startRecordVoice();
        }
    }

    private void startRecordVoice() {
        try {
            Speech.getInstance().startListening(this);
        } catch (SpeechRecognitionNotAvailable speechRecognitionNotAvailable) {
            speechRecognitionNotAvailable.printStackTrace();
        } catch (GoogleVoiceTypingDisabledException e) {
            e.printStackTrace();
        }
    }

    private void loadGoogleApiClient() {
        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();
    }

    private void loadGoogleMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        this.map = map;

        boolean success = map.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.map_style_json)));
        if (!success)
//            map.setMaxZoomPreference(16);
            map.setMapType(GoogleMap.MAP_TYPE_NONE);
        map.setMaxZoomPreference(20);
        map.setInfoWindowAdapter(new PlaceInfoWindow(this));

        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setMapToolbarEnabled(false);
        uiSettings.setCompassEnabled(false);

        TileProvider tileProvider = new CustomUrlTileProvider(512, 512);
        tileOverlay = map.addTileOverlay(new TileOverlayOptions()
                .tileProvider(tileProvider).zIndex(20));
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (!marker.getTitle().equals("Ubicación"))
                    loadARCameraActivity();
            }
        });
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                if (placePosMarker == null)
                    placePosMarker = map.addMarker(new MarkerOptions().position(latLng)
                            .title(String.valueOf(String.valueOf(latLng.latitude)) + ", " + String.valueOf(latLng.longitude))
                            .snippet("Sitio seleccionado"));
                else {
                    placePosMarker.setPosition(latLng);
                    placePosMarker.setTitle(String.valueOf(String.valueOf(latLng.latitude)) + ", " + String.valueOf(latLng.longitude));
                    placePosMarker.setSnippet("Sitio seleccionado");
                }
                APIManager.getRouteService().getRoute(String.valueOf(userLocation.latitude) +
                                "," + String.valueOf(userLocation.longitude),
                        String.valueOf(latLng.latitude) + ","
                                + String.valueOf(latLng.longitude), accesibleChecked)
                        .enqueue(new Callback<RouteResponse>() {
                            @Override
                            public void onResponse(Call<RouteResponse> call, Response<RouteResponse> response) {
                                Log.i(TAG, "onResponse: " + response.code());
                                if (response.isSuccessful()) {
                                    Route route = response.body().getRoute();
                                    List<Leg> legs = route.getLegs();
                                    if (legs != null && legs.size() > 0) {
                                        String from = call.request().url().queryParameter("from");
                                        String to = call.request().url().queryParameter("to");
                                        String[] fromLatLng = from.split(",");
                                        String[] toLatLng = to.split(",");
                                        LatLng a = new LatLng(Double.valueOf(fromLatLng[0]), Double.valueOf(fromLatLng[1]));
                                        LatLng b = new LatLng(Double.valueOf(toLatLng[0]), Double.valueOf(toLatLng[1]));
                                        Leg leg = legs.get(0);
                                        drawRoute(a, b, leg.getManeuvers());
                                        notifyRouteToSmartWear(a, b, leg.getManeuvers());
                                    }

                                }
                            }

                            @Override
                            public void onFailure(Call<RouteResponse> call, Throwable t) {
                                Log.i(TAG, "onFailure: " + t.getMessage());
                            }
                        });
            }
        });
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(39.478896, -6.34246), 16));
    }

    void loadARCameraActivity() {
        Log.i(TAG, "loadARCameraActivity: ");
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "loadARCameraActivity: need to give permission");
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_SERVICE);
        } else {
            Intent cameraActivity = new Intent(getApplicationContext(), CustomCameraActivity.class);
            cameraActivity.putExtra("route", serializedRouteForAR);
            startActivity(cameraActivity);
        }

    }

    private void drawRoute(LatLng a, LatLng b, List<Maneuver> maneuvers) {
        List<LatLng> routePoints = new ArrayList<>();
        routePoints.add(a);
        for (Maneuver maneuver : maneuvers) {
            StartPoint startPoint = maneuver.getStartPoint();
            routePoints.add(new LatLng(startPoint.getLat(), startPoint.getLng()));
            Log.i(TAG, "drawRoute: " + startPoint.getLat() + "," + startPoint.getLng());
        }
        routePoints.add(b);
        if (routePolilyne != null)
            routePolilyne.remove();
        routePolilyne = map.addPolyline(new PolylineOptions()
                .clickable(true)
                .addAll(routePoints));
        routePolilyne.setZIndex(1000);
    }

    private void notifyRouteToSmartWear(LatLng a, LatLng b, List<Maneuver> maneuvers) {
        int count = 0;
        List<RoutePoint> waypoints = new ArrayList<>();
        List<RoutePointWithIndication> waypointWithIndications = new ArrayList<>();
        for (Maneuver maneuver : maneuvers) {
            waypoints.add(new RoutePoint(maneuver.getStartPoint().getLat(), maneuver.getStartPoint().getLng()));
            waypointWithIndications.add(new RoutePointWithIndication(maneuver.getStartPoint().getLat(), maneuver.getStartPoint().getLng(), maneuver.getNarrative()));
        }
        RoutePoints routePoints = new RoutePoints(new RoutePoint(a.latitude, a.longitude),
                new RoutePoint(b.latitude, b.longitude), waypoints);
        RoutePointsWithIndication routePointsWithIndication = new RoutePointsWithIndication(new RoutePointWithIndication(a.latitude, a.longitude, "Inicio"),
                new RoutePointWithIndication(b.latitude, b.longitude, "fin"), waypointWithIndications);
        serializedRoute = new Gson().toJson(routePoints);
        serializedRouteForAR = new Gson().toJson(routePointsWithIndication);
        new StartWearableActivityTask().execute(serializedRoute);
    }

    @OnClick(R.id.route_button)
    public void onRoutePressed() {
        Log.i(TAG, "onRoutePressed: ");
        //new StartWearableActivityTask().execute();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            String[] accessCoarseLocationPermission = {Manifest.permission.RECORD_AUDIO};
            ActivityCompat.requestPermissions(this, accessCoarseLocationPermission, PERMISSION_REQUEST_RECORD_AUDIO);
        } else
            startRecordVoice();
        autocompleteFragment.setText("");
    }

    @WorkerThread
    private Collection<String> getNodes() {
        HashSet<String> results = new HashSet<>();

        Task<List<Node>> nodeListTask =
                Wearable.getNodeClient(getApplicationContext()).getConnectedNodes();

        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            List<Node> nodes = Tasks.await(nodeListTask);

            for (Node node : nodes) {
                results.add(node.getId());

            }

        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);

        } catch (InterruptedException exception) {
            Log.e(TAG, "Interrupt occurred: " + exception);
        }

        return results;
    }

    @WorkerThread
    private void sendStartActivityMessage(String node) {

        Task<Integer> sendMessageTask =
                Wearable.getMessageClient(this).sendMessage(node, START_ACTIVITY_PATH, new byte[0]);

        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            Integer result = Tasks.await(sendMessageTask);
            Log.i(TAG, "Message sent: " + result);

        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);

        } catch (InterruptedException exception) {
            Log.e(TAG, "Interrupt occurred: " + exception);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
    }

    public PlaceMarker getPlaceMarker() {
        return placeMarker;
    }

    public boolean getLocationLoaded() {
        return locationLoaded;
    }

    public void setLocationLoaded(boolean locationLoaded) {
        this.locationLoaded = locationLoaded;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getLatLng());
                placeMarker = new PlaceMarker(false, place.getLatLng());
                if (placePosMarker == null) {
                    placePosMarker = map.addMarker(new MarkerOptions()
                            .position(place.getLatLng())
                            .title(place.getName().toString())
                            .snippet(place.getAddress().toString())
                    );
                    placePosMarker.showInfoWindow();
                } else {
                    placePosMarker.setPosition(place.getLatLng());
                    placePosMarker.setTitle(place.getName().toString());
                    placePosMarker.setSnippet(place.getAddress().toString());
                    placePosMarker.showInfoWindow();
                }
                APIManager.getRouteService().getRoute(String.valueOf(userLocation.latitude) +
                                "," + String.valueOf(userLocation.longitude),
                        String.valueOf(place.getLatLng().latitude) + ","
                                + String.valueOf(place.getLatLng().longitude), accesibleChecked)
                        .enqueue(new Callback<RouteResponse>() {
                            @Override
                            public void onResponse(Call<RouteResponse> call, Response<RouteResponse> response) {
                                Log.i(TAG, "onResponse: " + response.code());
                                if (response.isSuccessful()) {
                                    Route route = response.body().getRoute();
                                    List<Leg> legs = route.getLegs();
                                    if (legs != null && legs.size() > 0) {
                                        String from = call.request().url().queryParameter("from");
                                        String to = call.request().url().queryParameter("to");
                                        String[] fromLatLng = from.split(",");
                                        String[] toLatLng = to.split(",");
                                        LatLng a = new LatLng(Double.valueOf(fromLatLng[0]), Double.valueOf(fromLatLng[1]));
                                        LatLng b = new LatLng(Double.valueOf(toLatLng[0]), Double.valueOf(toLatLng[1]));
                                        Leg leg = legs.get(0);
                                        drawRoute(a, b, leg.getManeuvers());
                                        notifyRouteToSmartWear(a, b, leg.getManeuvers());
                                    }

                                }
                            }

                            @Override
                            public void onFailure(Call<RouteResponse> call, Throwable t) {
                                Log.i(TAG, "onFailure: " + t.getMessage());
                            }
                        });
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16));
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onStartOfSpeech() {

    }

    @Override
    public void onSpeechRmsChanged(float value) {

    }

    @Override
    public void onSpeechPartialResults(List<String> results) {

    }

    @Override
    public void onSpeechResult(String result) throws SecurityException {
        if (result.isEmpty()) {
            Toast.makeText(this, "Inténtalo de nuevo", Toast.LENGTH_SHORT).show();
        } else {
            if (result.replace(" ", "").toLowerCase()
                    .replace("á", "a")
                    .replace("é", "e")
                    .replace("í", "i")
                    .replace("ó", "o")
                    .replace("ú", "u")
                    .equals("dondeestoy")) {
                Log.i(TAG, "onSpeechResult: estoy");
                String coordinates = String.valueOf(userLocation.latitude) + "," + String.valueOf(userLocation.longitude);
                PendingResult<PlaceLikelihoodBuffer> results =
                        Places.PlaceDetectionApi.getCurrentPlace(googleApiClient, null);
                results.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {
                        if (placeLikelihoods.getStatus().isSuccess()) {
                            Speech.getInstance().say("Estás en " + placeLikelihoods.get(0).getPlace().getAddress());
                        }
                    }
                });
            } else {
                Speech.getInstance().say("Tus deseos son órdenes");
                autocompleteFragment.setText(result);
                PendingResult<AutocompletePredictionBuffer> results =
                        Places.GeoDataApi.getAutocompletePredictions(googleApiClient, result, LatLngBoundsCalculator.boundsWithCenterAndLatLngDistance(userLocation, 50000, 50000), null);
                results.setResultCallback(new ResultCallback<AutocompletePredictionBuffer>() {
                    @Override
                    public void onResult(@NonNull AutocompletePredictionBuffer autocompletePredictions) {
                        try {
                            final Status status = autocompletePredictions.getStatus();
                            if (status.isSuccess()) {
                                if (autocompletePredictions.get(0) != null) {
                                    Places.GeoDataApi.getPlaceById(googleApiClient, autocompletePredictions.get(0).getPlaceId())
                                            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                                @Override
                                                public void onResult(@NonNull PlaceBuffer places) {
                                                    if (places != null && places.get(0) != null) {
                                                        Log.i(TAG, "onResult: " + places.get(0).getLatLng());
                                                        Place place = places.get(0);
                                                        placeMarker = new PlaceMarker(false, place.getLatLng());
                                                        if (placePosMarker == null) {
                                                            placePosMarker = map.addMarker(new MarkerOptions()
                                                                    .position(place.getLatLng())
                                                                    .title(place.getName().toString())
                                                                    .snippet(place.getAddress().toString())
                                                            );
                                                            placePosMarker.showInfoWindow();
                                                        } else {
                                                            placePosMarker.setPosition(place.getLatLng());
                                                            placePosMarker.setTitle(place.getName().toString());
                                                            placePosMarker.setSnippet(place.getAddress().toString());
                                                            placePosMarker.showInfoWindow();
                                                        }
                                                        APIManager.getRouteService().getRoute(String.valueOf(userLocation.latitude) +
                                                                        "," + String.valueOf(userLocation.longitude),
                                                                String.valueOf(place.getLatLng().latitude) + ","
                                                                        + String.valueOf(place.getLatLng().longitude), accesibleChecked)
                                                                .enqueue(new Callback<RouteResponse>() {
                                                                    @Override
                                                                    public void onResponse(Call<RouteResponse> call, Response<RouteResponse> response) {
                                                                        Log.i(TAG, "onResponse: " + response.code());
                                                                        if (response.isSuccessful()) {
                                                                            Route route = response.body().getRoute();
                                                                            List<Leg> legs = route.getLegs();
                                                                            if (legs != null && legs.size() > 0) {
                                                                                String from = call.request().url().queryParameter("from");
                                                                                String to = call.request().url().queryParameter("to");
                                                                                String[] fromLatLng = from.split(",");
                                                                                String[] toLatLng = to.split(",");
                                                                                LatLng a = new LatLng(Double.valueOf(fromLatLng[0]), Double.valueOf(fromLatLng[1]));
                                                                                LatLng b = new LatLng(Double.valueOf(toLatLng[0]), Double.valueOf(toLatLng[1]));
                                                                                Leg leg = legs.get(0);
                                                                                drawRoute(a, b, leg.getManeuvers());
                                                                                notifyRouteToSmartWear(a, b, leg.getManeuvers());
                                                                            }

                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<RouteResponse> call, Throwable t) {
                                                                        Log.i(TAG, "onFailure: " + t.getMessage());
                                                                    }
                                                                });
                                                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16));
                                                    }
                                                }
                                            });
                                }
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "onResult: " + e.getMessage());
                        }
                    }
                });
            }
        }
    }

    private class StartWearableActivityTask extends AsyncTask<String, Void, Void> {

        private String route;

        @Override
        protected Void doInBackground(String... args) {
            Collection<String> nodes = getNodes();
            for (String node : nodes) {
                sendStartActivityMessage(node);
            }
            this.route = args[0];
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(WAYPOINTS_PATH);
            Log.i(TAG, "notifyRouteToSmartWear: " + serializedRoute);
            putDataMapRequest.getDataMap().putLong("system", System.currentTimeMillis());
            putDataMapRequest.getDataMap().putString(WAYPOINT_ROUTE_KEY, serializedRoute);
            PutDataRequest putDataRequest = putDataMapRequest.asPutDataRequest();
            putDataRequest.setUrgent();
            Task<DataItem> putDataTask = Wearable.getDataClient(getBaseContext()).putDataItem(putDataRequest);
            putDataTask.addOnSuccessListener(new OnSuccessListener<DataItem>() {
                @Override
                public void onSuccess(DataItem dataItem) {
                    Log.i(TAG, "onSuccess: ");
                }
            });
            putDataTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.i(TAG, "onFailure: " + e.getStackTrace());
                }
            });
        }
    }
}