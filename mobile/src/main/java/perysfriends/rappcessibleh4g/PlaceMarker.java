package perysfriends.rappcessibleh4g;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by victor on 8/03/18.
 */

public class PlaceMarker {
    private boolean imageLoaded;
    private LatLng position;

    public PlaceMarker(boolean imageLoaded, LatLng position) {
        this.imageLoaded = imageLoaded;
        this.position = position;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public boolean isImageLoaded() {
        return imageLoaded;
    }

    public void setImageLoaded(boolean imageLoaded) {
        this.imageLoaded = imageLoaded;
    }
}
