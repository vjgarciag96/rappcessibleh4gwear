package perysfriends.rappcessibleh4g;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.DismissOverlayView;
import android.support.wearable.view.WatchViewStub;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import perysfriends.rappcessibleh4g.model.RoutePoint;
import perysfriends.rappcessibleh4g.model.RoutePoints;

public class MainActivity extends WearableActivity implements OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener,
        DataClient.OnDataChangedListener {

    private static final String TAG = "MainActivity";

    private static final String WAYPOINTS_PATH = "/waypoint";
    private static final String WAYPOINT_ROUTE_KEY = "waypoint-route-lat";

    private static final LatLng CACERES = new LatLng(39.478896, -6.34246);

    private DismissOverlayView mDismissOverlay;

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;

    private MapFragment mMapFragment;
    private Marker userLocationMarker;
    private RoutePoints routePoints;

    private Polyline routePolyline;

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setupViews();
        loadGoogleMap();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Wearable.getDataClient(this).addListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Wearable.getDataClient(this).removeListener(this);
    }

    private void loadGoogleMap() {
        mMapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
    }

    private void setupViews() {
        setAmbientEnabled();

        final FrameLayout topFrameLayout = findViewById(R.id.root_container);
        final FrameLayout mapFrameLayout = findViewById(R.id.map_container);

        topFrameLayout.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
            @Override
            public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                insets = topFrameLayout.onApplyWindowInsets(insets);

                FrameLayout.LayoutParams params =
                        (FrameLayout.LayoutParams) mapFrameLayout.getLayoutParams();

                params.setMargins(
                        insets.getSystemWindowInsetLeft(),
                        insets.getSystemWindowInsetTop(),
                        insets.getSystemWindowInsetRight(),
                        insets.getSystemWindowInsetBottom());
                mapFrameLayout.setLayoutParams(params);

                return insets;
            }
        });

        mDismissOverlay = (DismissOverlayView) findViewById(R.id.dismiss_overlay);
        mDismissOverlay.setIntroText(R.string.intro_text);
        mDismissOverlay.showIntroIfNecessary();
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        mMapFragment.onEnterAmbient(ambientDetails);
    }

    @Override
    public void onExitAmbient() {
        super.onExitAmbient();
        mMapFragment.onExitAmbient();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(this);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CACERES, 10));
        drawRoute(routePoints);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mDismissOverlay.show();
    }

    @Override
    public void onDataChanged(@NonNull DataEventBuffer dataEventBuffer) {
        Log.i(TAG, "onDataChanged: ");
        for (DataEvent event : dataEventBuffer) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().equals(WAYPOINTS_PATH)) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    String serializedRoute = dataMap.getString(WAYPOINT_ROUTE_KEY);
                    routePoints = new Gson().fromJson(serializedRoute, RoutePoints.class);
                    if(mMap != null && routePoints != null)
                        drawRoute(routePoints);
                }
            }
        }
    }

    private void drawRoute(RoutePoints routePoints){
        if(routePoints != null) {
            List<LatLng> route = new ArrayList<>();
            for (RoutePoint point : routePoints.getWaypoints())
                route.add(new LatLng(point.getLatitude(), point.getLongitude()));
            route.add(new LatLng(routePoints.getEnd().getLatitude(), routePoints.getEnd().getLongitude()));
            if (routePolyline != null)
                routePolyline.remove();
            routePolyline = mMap.addPolyline(new PolylineOptions()
                    .clickable(true)
                    .addAll(route));
            userLocationMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)).position(route.get(0)).zIndex(1000).anchor(0.5F, 0.5F));

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(route.get(0), 16));
        }
    }
}
