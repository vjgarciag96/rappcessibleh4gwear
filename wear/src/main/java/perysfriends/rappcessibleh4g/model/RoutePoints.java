package perysfriends.rappcessibleh4g.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by victor on 5/03/18.
 */

public class RoutePoints implements Serializable {

    private RoutePoint start;
    private RoutePoint end;
    private List<RoutePoint> waypoints;

    public RoutePoints(RoutePoint start, RoutePoint end, List<RoutePoint> waypoints) {
        this.start = start;
        this.end = end;
        this.waypoints = waypoints;
    }

    public RoutePoint getStart() {
        return start;
    }

    public void setStart(RoutePoint start) {
        this.start = start;
    }

    public RoutePoint getEnd() {
        return end;
    }

    public void setEnd(RoutePoint end) {
        this.end = end;
    }

    public List<RoutePoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<RoutePoint> waypoints) {
        this.waypoints = waypoints;
    }
}
